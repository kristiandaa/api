package com.price.api.controller;

import com.price.api.dto.PriceGetDto;
import com.price.api.service.PriceService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;



@RestController
@RequestMapping("/v1/product")
public class PriceController {
    @Autowired
    PriceService service;
    @Operation(summary = "Get Product")
    @ApiResponses(value ={
            @ApiResponse(responseCode = "200",description = "Success"),
            @ApiResponse(responseCode = "404",description = "Not found")})
    @GetMapping("")
    public ResponseEntity<PriceGetDto> search(@RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) String dateRequest,
                                                @RequestParam String product,
                                                @RequestParam String brand)  {
        return new ResponseEntity<>(new PriceGetDto(service.search(dateRequest,product,brand)), HttpStatus.OK);
    }
}
