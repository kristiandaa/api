package com.price.api.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;


import java.io.Serializable;

import java.util.Date;

@Getter
@Setter
@Entity
@Table(name="prices")
public class Price implements Serializable {
    @Id
    private String id;

    @Column(name ="brand_id")
    private String brandId;

    @Column (name = "start_date")
    private Date startDate;

    @Column(name = "end_date")
    private Date endDate;


    @Column(name ="price_list")
    private String priceList;

    @Column(name ="product_id")
    private String productId;

    private int priority;

    private Double price;

    private String curr;
}
