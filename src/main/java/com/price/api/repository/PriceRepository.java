package com.price.api.repository;

import com.price.api.model.Price;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.Date;

public interface PriceRepository  extends JpaRepository<Price,String> {
    @Query("Select p from Price p where p.productId =:productId and p.brandId =:brandId and :dateRequest >= p.startDate  and :dateRequest <=p.endDate ")
    Collection<Price> findPriceByProductIdAndBrandIdAndStartDate(String productId, String brandId, Date dateRequest);
}
