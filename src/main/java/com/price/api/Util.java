package com.price.api;

import com.price.api.exception.BadRequestException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Util {
    public static Date transformDate(String dateRequest){


        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            formatter.setLenient(false);
            return formatter.parse(dateRequest);
        } catch (ParseException e) {
            throw new BadRequestException("Fecha no valida "+dateRequest);
        }
    }
}
