package com.price.api.service;

import com.price.api.Util;

import com.price.api.exception.NotFoundException;
import com.price.api.model.Price;
import com.price.api.repository.PriceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.Collection;
import java.util.Comparator;

import java.util.Optional;
@Service
public class PriceService {

    @Autowired
    private PriceRepository repository;
    public Price findById (String id){
        Optional<Price> priceOptional =  repository.findById(id);

        return priceOptional.get();
    }

    public Price search (String dateRequest,String product,String brand) {

        Collection<Price> response = repository.findPriceByProductIdAndBrandIdAndStartDate(product, brand, Util.transformDate(dateRequest));
        if (!response.isEmpty()) {
            return response.stream().max(Comparator.comparingInt(Price::getPriority)).get();
        } else {
            throw new NotFoundException("No existe informacion con los parametros de busqueda ");
        }

    }

}
