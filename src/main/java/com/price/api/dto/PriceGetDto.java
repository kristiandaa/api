package com.price.api.dto;

import com.price.api.model.Price;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class PriceGetDto implements Serializable {
    private String productId;
    private String brandId;
    private  String priceList;
    private Date startDate;
    private Date endDate;

    private Double price;


    public PriceGetDto(Price price){
      this.productId = price.getProductId();
      this.brandId=price.getBrandId();
      this.priceList=price.getPriceList();
      this.startDate=price.getStartDate();
      this.endDate=price.getEndDate();
      this.price = price.getPrice();

    }
}
